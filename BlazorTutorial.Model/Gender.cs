﻿using System;

namespace BlazorTutorial.Model
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}
