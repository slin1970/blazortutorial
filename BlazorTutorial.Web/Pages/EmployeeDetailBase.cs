﻿using BlazorTutorial.Model;
using BlazorTutorial.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorTutorial.Web.Pages
{
    public class EmployeeDetailBase : ComponentBase
    {
        [Inject] public IEmployeeService EmployeeService { get; set; }

        [Parameter] public string Id { get; set; }
        protected string Coordinates { get; set; }
        protected string Button_Text { get; set; } = "Hide Footer";
        protected string CssClass { get; set; } = string.Empty;

        public Employee Employee { get; set; } = new Employee();
        protected override async Task OnInitializedAsync()
        {
            //return base.OnInitializedAsync();
            Id = Id ?? "1";
            Employee = await EmployeeService.GetEmployee(int.Parse(Id));
        }

        protected void Mouse_Move(MouseEventArgs e)
        {
            Coordinates = $"X = {e.ClientX} Y = {e.ClientY}";
        }

        protected void Button_Click()
        {
            if (Button_Text == "Hide Footer")
            {
                Button_Text = "Show Footer";
                CssClass = "HideFooter";
            }
            else
            {
                Button_Text = "Hide Footer";
                CssClass = string.Empty;
            }
        }
    }
}
