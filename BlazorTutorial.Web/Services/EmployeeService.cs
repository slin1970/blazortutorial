﻿using BlazorTutorial.Model;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlazorTutorial.Web.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly HttpClient httpClient;

        public EmployeeService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            var result = await httpClient.GetJsonAsync<Employee[]>("api/employees");
            return result;
        }

        public async Task<Employee> GetEmployee(int id)
        {
            var result = await httpClient.GetJsonAsync<Employee>($"api/employees/{id}");
            return result;
        }

        public async Task<Employee> UpdateEmployee(Employee employee)
        {
            var result = await httpClient.PutJsonAsync<Employee>("api/employees", employee);
            return result;
        }

        public async Task<Employee> CreateEmployee(Employee employee)
        {
            var result = await httpClient.PostJsonAsync<Employee>("api/employees", employee);
            return result;
        }
    }
}
