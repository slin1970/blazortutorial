﻿using BlazorTutorial.Model;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlazorTutorial.Web.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly HttpClient httpClient;

        public DepartmentService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<Department> GetDepartment(int id)
        {
            var result = await httpClient.GetJsonAsync<Department>($"api/departments/{id}");
            return result;
        }

        public async Task<IEnumerable<Department>> GetDepartments()
        {
            var result = await httpClient.GetJsonAsync<IEnumerable<Department>>("api/departments");
            return result;
        }
    }
}
